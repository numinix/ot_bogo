<?php
/**
 * ot_total order-total module
 *
 * @package orderTotal
 * @copyright Copyright 2003-2007 Zen Cart Development Team
 * @copyright Portions Copyright 2003 osCommerce
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 * @version $Id: ot_bogo.php 7 2012-02-26 01:57:31Z numinix $
 */
 
  // Note: The purchased product is the product purchased in order to receive a discount on another product.   
  class ot_bogo {                          
    var $title, $output, $code, $description, $sort_order, $enabled;

    function __construct() {

      $this->code = 'ot_bogo';
      $this->title = MODULE_ORDER_TOTAL_BOGO_TITLE;
      $this->description = MODULE_ORDER_TOTAL_BOGO_DESCRIPTION;
      $this->sort_order = defined('MODULE_ORDER_TOTAL_BOGO_SORT_ORDER') ? (int)MODULE_ORDER_TOTAL_BOGO_SORT_ORDER : null;;
      $this->enabled = defined('MODULE_ORDER_TOTAL_BOGO_ENABLED') ? MODULE_ORDER_TOTAL_BOGO_ENABLED :  define('MODULE_ORDER_TOTAL_BOGO_ENABLED', 'false');
      $this->output = array();
 
 			// scheduling
      global $db;		
      if (defined("MODULE_ORDER_TOTAL_BOGO_END_TIME") && defined("MODULE_ORDER_TOTAL_BOGO_ZONE")) {
        switch(true) {
          // discount is inactive and the time is greater than the start time but less than the end time
          case (MODULE_ORDER_TOTAL_BOGO_ENABLED == 'false' && (MODULE_ORDER_TOTAL_BOGO_END_TIME != '' && strtotime(MODULE_ORDER_TOTAL_BOGO_START_TIME) <= time()) && (MODULE_ORDER_TOTAL_BOGO_END_TIME == '' || strtotime(MODULE_ORDER_TOTAL_BOGO_END_TIME) > time())):
            // enable the discount
            $db->Execute("UPDATE " . TABLE_CONFIGURATION . " SET configuration_value = 'true' WHERE configuration_key = 'MODULE_ORDER_TOTAL_BOGO_ENABLED' LIMIT 1;");				
            break;
          // discount is active and the time is greater than the end time
          case (MODULE_ORDER_TOTAL_BOGO_ENABLED == 'true' && MODULE_ORDER_TOTAL_BOGO_END_TIME != '' && strtotime(MODULE_ORDER_TOTAL_BOGO_END_TIME) <= time()):
            // disable the discount
            $db->Execute("UPDATE " . TABLE_CONFIGURATION . " SET configuration_value = 'false' WHERE configuration_key = 'MODULE_ORDER_TOTAL_BOGO_ENABLED' LIMIT 1;");
            $this->enabled = false;
            break;
        }
              
        if ($this->enabled == true) {
          if (MODULE_ORDER_TOTAL_BOGO_ZONE > 0) {
            global $order;
            $this->enabled = false;
            $check = $db->Execute("select zone_id from " . TABLE_ZONES_TO_GEO_ZONES . " where geo_zone_id = '" . MODULE_ORDER_TOTAL_BOGO_ZONE . "' and zone_country_id = '" . $order->delivery['country']['id'] . "' order by zone_id");
            while (!$check->EOF) {
              if ( ($check->fields['zone_id'] < 1) || ($check->fields['zone_id'] == $order->delivery['zone_id']) ) {
                $this->enabled = true;
                break;
              }
              $check->MoveNext();
            } // end while
          }
        }
      }
    }

    function process() {
      global $order, $currencies, $db;

      if ($this->enabled == true && (!isset($_SESSION['cc_id']) || MODULE_ORDER_TOTAL_BOGO_COUPONS == 'true')) {
        $pass = false;
        // add any specials requirements here and assign value to $pass
        if ((MODULE_ORDER_TOTAL_BOGO_FACEBOOK == 'false' || $_SESSION['facebook_like'])) {
          // check how many bogo orders have been placed today
          //$bogo_query = $db->Execute("SELECT customers_id FROM bogo WHERE customers_id = " . $_SESSION['customer_id'] . " LIMIT 1;");
          if (isset($_SESSION['customer_id'])) {
            $bogo_query = "SELECT o.orders_id FROM " . TABLE_ORDERS . " o 
                           LEFT JOIN " . TABLE_ORDERS_TOTAL . " ot ON (ot.orders_id = o.orders_id)
                           WHERE DATE_FORMAT(FROM_UNIXTIME(o.date_purchased),'%m-%d') = DATE_FORMAT(NOW(),'%m-%d')
                           AND o.customers_id = " . (int)$_SESSION['customer_id'] . " 
                           AND ot.class = 'ot_bogo';";
            $bogo = $db->Execute($bogo_query);
            if (($bogo->RecordCount() == 0) || ($bogo->RecordCount() < (int)MODULE_ORDER_TOTAL_BOGO_DAILY_LIMIT)) {
              $pass = true; // default
            }
          } else {
            $pass = true; // default
          }
        }
        if (($pass == true)) {
          $tax_address = zen_get_tax_locations();
          $tax = zen_get_tax_rate(MODULE_ORDER_TOTAL_BOGO_TAX_CLASS, $tax_address['country_id'], $tax_address['zone_id']);
          //$tax_description = zen_get_tax_description(MODULE_ORDER_TOTAL_BOGO_TAX_CLASS, $tax_address['country_id'], $tax_address['zone_id']);
          $tax_description = zen_get_tax_description(MODULE_ORDER_TOTAL_BOGO_TAX_CLASS);
          
          // create an array of all products
          $products = $_SESSION['cart']->get_products();
          $products_array = $ineligible_products_array = array();
          $eligible_categories = explode(',', str_replace(' ', '', MODULE_ORDER_TOTAL_BOGO_CATEGORIES));
          $ineligible_categories = explode(',', str_replace(' ', '', MODULE_ORDER_TOTAL_BOGO_NEG_CATEGORIES));
          $count = 0;
          
          // prepare for specials/sales
          if (MODULE_ORDER_TOTAL_BOGO_SPECIALS == 'false') {
            // get all active sales
            $sales_query = "SELECT sale_categories_all FROM " . TABLE_SALEMAKER_SALES . " WHERE sale_status = 1";
            $sales = $db->Execute($sales_query);             
          }
          
          foreach ($products as $product) {
          	$is_ineligible = false;
            for ($i=1; $i<=$product['quantity']; $i++) {
              $count++;
              $continue = false;
              
              // check if product meets the category restrictions
              if (trim(MODULE_ORDER_TOTAL_BOGO_CATEGORIES) != '' || trim(MODULE_ORDER_TOTAL_BOGO_NEG_CATEGORIES) != '') {
                if (trim(MODULE_ORDER_TOTAL_BOGO_CATEGORIES) != '') {
                  foreach($eligible_categories as $category) {
                    if (zen_product_in_category($product['id'], trim((int)$category))) {
                      $continue = true;
                      break;
                    }
                  }
                } else {
                  $continue = true;
                }
                if (trim(MODULE_ORDER_TOTAL_BOGO_NEG_CATEGORIES) != '') {
                  foreach($ineligible_categories as $category) {
                    if (zen_product_in_category($product['id'], trim((int)$category))) {
                    	if (MODULE_ORDER_TOTAL_BOGO_INELIGIBLE_ELIGIBLE == 'true') { // check to see if ineligible products can still trigger the BOGO
                    		$is_ineligible = true;
                    		$continue = true;
						} else {
                      	  $continue = false;
						}
                      	break;
                    }
                  }
                }
              } else {
                $continue = true;
              }
              
              if ($continue && MODULE_ORDER_TOTAL_BOGO_SPECIALS == 'false') {
                // check if product is on special
                $specials_query = "SELECT * FROM " . TABLE_SPECIALS . " WHERE products_id = " . (int)$product['id'] . " AND status = 1 LIMIT 1";
                $specials = $db->Execute($specials_query);
                if ($specials->RecordCount() > 0) {
                  $continue = false;
                }
                if ($sales->RecordCount() > 0) {
                  // check if product is on sale
                  $sales_obj = $sales; // set to value of original object
                  while (!$sales_obj->EOF) {
                    $sale_categories_all = explode(',', $sales_obj->fields['sale_categories_all']);
                    $sale_categories_all = array_filter($sale_categories_all); // should remove empty elements
                    foreach ($sale_categories_all as $sale_category) {
                      if (zen_product_in_category($product['id'], $sale_category)) {
                        //echo $sale_category . ' ';
                        $continue = false;
                        break 2;
                      } 
                    }
                    $sales_obj->MoveNext();
                  }
                }
              }
              
              if ($continue) {
                // check if product meets the price restrictions
                if ( ((float)MODULE_ORDER_TOTAL_BOGO_MINIMUM > 0) || ((float)MODULE_ORDER_TOTAL_BOGO_MAXIMUM > 0) ) {
                  if ( ((float)MODULE_ORDER_TOTAL_BOGO_MINIMUM > 0) && ((float)MODULE_ORDER_TOTAL_BOGO_MAXIMUM > 0) ) {
                    if ( ($product['final_price'] < (float)MODULE_ORDER_TOTAL_BOGO_MINIMUM) || ($product['final_price'] > (float)MODULE_ORDER_TOTAL_BOGO_MAXIMUM)) $continue = false;
                  } elseif ((float)MODULE_ORDER_TOTAL_BOGO_MINIMUM > 0 && ($product['final_price'] < (float)MODULE_ORDER_TOTAL_BOGO_MINIMUM)) {
                    $continue = false;
                  } elseif ( ((float)MODULE_ORDER_TOTAL_BOGO_MAXIMUM > 0) && ($product['final_price'] > (float)MODULE_ORDER_TOTAL_BOGO_MAXIMUM)) {
                    $continue = false;
                  } 
                }
              }
                
              if ($continue) {
              	if ($is_ineligible) {
              		$ineligible_products_array[$count] = array('price' => $product['final_price'], 'id' => (int)$product['id']);
								} else {
                	$products_array[$count] = array('price' => $product['final_price'], 'id' => (int)$product['id']);
								}
              }
            }
          }
          

          if ((sizeof($products_array) + sizeof($ineligible_products_array)) > MODULE_ORDER_TOTAL_BOGO_QUANTITY) {
            arsort($products_array); // sort array high to low
            arsort($ineligible_products_array); // sort array high to low
            //$products_array = array_values($products_array);
            //echo '<pre>';
            //print_r($products_array);
            //die('</pre>');          
            $num_discounts = 0;
            $eligible_total = 0;
      		$match_found = true;
      		$limit_reached = false;
            while (sizeof($ineligible_products_array) > 0 && sizeof($products_array) > 0 && $match_found) {
            	$match_found = false;
            	foreach($ineligible_products_array as $counter => $values) {
            		foreach ($products_array as $counter2 => $values2) {
            			if ($values2['price'] <= $values['price']) {
            				$eligible_total += $values2['price'];
            				// remove items from both array and break inner loop
            				unset($products_array[$counter2]);
            				unset($ineligible_products_array[$counter]);
            				$match_found = true;
            				$num_discounts++;
            				if ($num_discounts == (int)MODULE_ORDER_TOTAL_BOGO_LIMIT) 
            				{
            					$limit_reached = true;
            					break 3;
							}
            				break;
						}
					}
				}
			}
			$count = 0;
			$last_item_is_bogo = false;
			if (!$limit_reached) {
	            foreach ($products_array as $counter => $values) {
	              $count++;
	              if ($count > MODULE_ORDER_TOTAL_BOGO_QUANTITY && !$last_item_is_bogo) {
	                $eligible_total += $values['price'];
	                $last_item_is_bogo = true;                                                         
	                $num_discounts++;
	                $count = 0;
	                if ($num_discounts == (int)MODULE_ORDER_TOTAL_BOGO_LIMIT) break;
	              } else {
	                $last_item_is_bogo = false;
	              }  
	            }
			}
           
            $discount_percentage = (float)MODULE_ORDER_TOTAL_BOGO_DISCOUNT / 100; // i.e. 1.00
            $discount = $discount_percentage * $eligible_total; // i.e. 1.00 * 50 = $50 discount 
            $tax_discount = zen_calculate_tax($discount, $tax);
                                
            if (!isset($_SESSION['customer_id'])) {
							$tax_discount = 0;
            }

            $order->info['tax'] -= $tax_discount;
            $order->info['tax_groups']["$tax_description"] -= $tax_discount;
            //echo '<pre>' , print_r($order->info['tax_groups']) , '</pre>';
            $order->info['total'] = $order->info['total'] - $discount - $tax_discount;
            if (DISPLAY_PRICE_WITH_TAX == 'true') {
              $discount += $tax_discount;
            }

            if ($discount > 0) {
              $this->output[] = array('title' => '<span class="red">' . MODULE_ORDER_TOTAL_BOGO_DISPLAY_TITLE . ':</span>',
                                      'text' => '-' . $currencies->format($discount, true, $order->info['currency'], $order->info['currency_value']),
                                      'value' => '-' . $discount);
            } else {
              return false;
            }
          } else {
            return false;
          }
        }
      }
    }
    
	function round_nearest($no,$near) {
		return round($no/$near)*$near;
	}     

    function check() {
      global $db;
      if (!isset($this->_check)) {
        $check_query = "select configuration_value
                        from " . TABLE_CONFIGURATION . "
                        where configuration_key = 'MODULE_ORDER_TOTAL_BOGO_STATUS'";

        $check_query = $db->Execute($check_query);
        $this->_check = $check_query->RecordCount();
      }
      
      if ($this->_check && defined('MODULE_ORDER_TOTAL_BOGO_VERSION')) { 
	      switch(MODULE_ORDER_TOTAL_BOGO_VERSION) {
	        case '1.1.0':
	          $db->Execute("UPDATE " . TABLE_CONFIGURATION . " SET configuration_value = '1.1.1' WHERE configuration_key = 'MODULE_ORDER_TOTAL_BOGO_VERSION' LIMIT 1;");
	          $current_version = '1.1.1';	          
	        case '1.1.1':
			  $db->Execute("insert ignore into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Start Time', 'MODULE_ORDER_TOTAL_BOGO_START_TIME', '', 'Enter a start time to allow this module to run or leave blank to turn on immediately (format: mm/dd/yyyy hh:mm:ss, example: 12/11/2013 00:00:00)', '6', '2', now())");
			  $db->Execute("insert ignore into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('End Time', 'MODULE_ORDER_TOTAL_BOGO_END_TIME', '', 'Enter an end time to turn off this module or leave blank to allow the module to run indefinitely (format: mm/dd/yyyy hh:mm:ss, example: 12/12/2013 00:00:00)', '6', '2', now())");
	          $db->Execute("UPDATE " . TABLE_CONFIGURATION . " SET configuration_value = '1.2.0' WHERE configuration_key = 'MODULE_ORDER_TOTAL_BOGO_VERSION' LIMIT 1;");
	          $current_version = '1.2.0';
	        case '1.2.0':
	          $db->Execute("UPDATE " . TABLE_CONFIGURATION . " SET configuration_value = '1.2.1' WHERE configuration_key = 'MODULE_ORDER_TOTAL_BOGO_VERSION' LIMIT 1;");
	          $current_version = '1.2.1';
	        case '1.2.1':
	          $db->Execute("UPDATE " . TABLE_CONFIGURATION . " SET configuration_value = '1.2.2' WHERE configuration_key = 'MODULE_ORDER_TOTAL_BOGO_VERSION' LIMIT 1;");
	          $current_version = '1.2.2';	          	
	        default:
	          $current_version = '1.2.2';
	          break;                                  
	      }
      }      

      return $this->_check;
    }

    function keys() {
      return array('MODULE_ORDER_TOTAL_BOGO_STATUS', 'MODULE_ORDER_TOTAL_BOGO_VERSION', 'MODULE_ORDER_TOTAL_BOGO_SORT_ORDER', 'MODULE_ORDER_TOTAL_BOGO_ENABLED', 'MODULE_ORDER_TOTAL_BOGO_DISPLAY_TITLE', 'MODULE_ORDER_TOTAL_BOGO_CATEGORIES', 'MODULE_ORDER_TOTAL_BOGO_NEG_CATEGORIES', 'MODULE_ORDER_TOTAL_BOGO_INELIGIBLE_ELIGIBLE', 'MODULE_ORDER_TOTAL_BOGO_DISCOUNT', 'MODULE_ORDER_TOTAL_BOGO_QUANTITY', 'MODULE_ORDER_TOTAL_BOGO_LIMIT', 'MODULE_ORDER_TOTAL_BOGO_MINIMUM', 'MODULE_ORDER_TOTAL_BOGO_MAXIMUM', 'MODULE_ORDER_TOTAL_BOGO_DAILY_LIMIT', 'MODULE_ORDER_TOTAL_BOGO_SPECIALS', 'MODULE_ORDER_TOTAL_BOGO_FACEBOOK', 'MODULE_ORDER_TOTAL_BOGO_COUPONS', 'MODULE_ORDER_TOTAL_BOGO_START_TIME', 'MODULE_ORDER_TOTAL_BOGO_END_TIME', 'MODULE_ORDER_TOTAL_BOGO_ZONE', 'MODULE_ORDER_TOTAL_BOGO_TAX_CLASS');
    }

    function install() {
      global $db;
      $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Module Status', 'MODULE_ORDER_TOTAL_BOGO_STATUS', 'true', 'Installed', '6', '1','zen_cfg_select_option(array(\'true\'), ', now())");
      $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Version', 'MODULE_ORDER_TOTAL_BOGO_VERSION', '1.2.2', 'Version Installed:', '6', '1', now())");
      $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Sort Order', 'MODULE_ORDER_TOTAL_BOGO_SORT_ORDER', '299', 'Sort order of display (set to display before ot_tax)', '6', '2', now())");
      $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Enable Module', 'MODULE_ORDER_TOTAL_BOGO_ENABLED', 'false', 'Do you want to enable this discount module?', '6', '3', 'zen_cfg_select_option(array(\'true\', \'false\'), ', now())");
      $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Title', 'MODULE_ORDER_TOTAL_BOGO_DISPLAY_TITLE', '', 'This will be displayed as the module name in the checkout summary', '6', '4', now())");      
      $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Eligible Categories', 'MODULE_ORDER_TOTAL_BOGO_CATEGORIES', '', 'Enter the category numbers separated by commas or leave blank to allow all products', '6', '10', now())");      
      $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Ineligible Categories', 'MODULE_ORDER_TOTAL_BOGO_NEG_CATEGORIES', '', 'Enter the category numbers separated by commas or leave blank to allow all products', '6', '10', now())");      
      $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Allow Ineligible Products to Trigger BOGO', 'MODULE_ORDER_TOTAL_BOGO_INELIGIBLE_ELIGIBLE', 'false', 'If enabled, inelgibile products can still be counted as the product of equal or greater value that allows a second item to be discounted.', '6', '11', 'zen_cfg_select_option(array(\'true\', \'false\'), ', now())");
      $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Discount Percentage', 'MODULE_ORDER_TOTAL_BOGO_DISCOUNT', '100', 'What percentage discount should be given for eligible purchases?', '6', '20', now())");
      $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Minimum Quantity Required', 'MODULE_ORDER_TOTAL_BOGO_QUANTITY', '1', 'How many eligible products must be purchased in order to get the discount on the next product?  For example, if you want to do a buy 4 get 1 free, enter 4 here and 100% as the discount.', '6', '20', now())");
      $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Discount Limit', 'MODULE_ORDER_TOTAL_BOGO_LIMIT', '1', 'How many discounted items can a customer order? Set to an impossibly high number for unlimited.', '6', '30', now())");
      $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Minimum Price', 'MODULE_ORDER_TOTAL_BOGO_MINIMUM', '0', 'What is the minimum price a product must have to be eligible for the BOGO?', '6', '35', now())");
      $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Maximum Price', 'MODULE_ORDER_TOTAL_BOGO_MAXIMUM', '999', 'What is the maximum price a product can have to be eligible for the BOGO?', '6', '36', now())");
      $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Daily Limit', 'MODULE_ORDER_TOTAL_BOGO_DAILY_LIMIT', '1', 'How many BOGO purchases can a customer make per day?  Set to a very high number if there is no limit.', '6', '37', now())");
      $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Facebook Like', 'MODULE_ORDER_TOTAL_BOGO_FACEBOOK', 'false', 'Is a Facebook Like required to activate the sale (Note: Facebook Like Connect module required)?', '6', '50', 'zen_cfg_select_option(array(\'true\', \'false\'), ', now())");
      $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Allow Coupons', 'MODULE_ORDER_TOTAL_BOGO_COUPONS', 'false', 'If set to false, the BOGO will not be given to orders containing coupons.', '6', '40', 'zen_cfg_select_option(array(\'true\', \'false\'), ', now())");
      $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Allow Specials/Sales Items', 'MODULE_ORDER_TOTAL_BOGO_SPECIALS', 'false', 'If set to false, sales/specials cannot be purchased to receive a discount on another product but can still be the discounted product themselves.', '6', '40', 'zen_cfg_select_option(array(\'true\', \'false\'), ', now())");
	    $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Start Time', 'MODULE_ORDER_TOTAL_BOGO_START_TIME', '', 'Enter a start time to allow this module to run or leave blank to turn on immediately (format: mm/dd/yyyy hh:mm:ss, example: 12/11/2013 00:00:00)', '6', '2', now())");
	    $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('End Time', 'MODULE_ORDER_TOTAL_BOGO_END_TIME', '', 'Enter an end time to turn off this module or leave blank to allow the module to run indefinitely (format: mm/dd/yyyy hh:mm:ss, example: 12/12/2013 00:00:00)', '6', '2', now())");
      $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, use_function, set_function, date_added) values ('Zone', 'MODULE_ORDER_TOTAL_BOGO_ZONE', '0', 'If a zone is selected, only enable this module for that zone', '6', '99', 'zen_get_zone_class_title', 'zen_cfg_pull_down_zone_classes(', now())");     
      $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, use_function, set_function, date_added) values ('Tax Class', 'MODULE_ORDER_TOTAL_BOGO_TAX_CLASS', '0', 'Use the following tax class on the discount.', '6', '99', 'zen_get_tax_class_title', 'zen_cfg_pull_down_tax_classes(', now())");
    }

    function remove() {
    global $db;
      $db->Execute("delete from " . TABLE_CONFIGURATION . " where configuration_key in ('" . implode("', '", $this->keys()) . "')");
    }
  }
?>